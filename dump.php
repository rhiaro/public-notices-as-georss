<?
include_once("lib/arc2/ARC2.php");

/* configuration */ 
$config = array(
  /* remote endpoint */
  'remote_store_endpoint' => 'http://opendata.tellmescotland.gov.uk/sparql',
);

/* instantiation */
$store = ARC2::getRemoteStore($config);

$q = "SELECT DISTINCT * WHERE {
  ?s ?p ?o
}
LIMIT 10";

$q2 = "PREFIX ispin: <http://opendata.tellmescotland.gov.uk/vocab/resource/> 
PREFIX georss: <http://www.georss.org/georss/>
SELECT ?s ?g
WHERE { 
?s rdf:type ispin:Notice . 
?s georss:point ?g .
}";

$q3 = "PREFIX georss: <http://www.georss.org/georss/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX ispin: <http://opendata.tellmescotland.gov.uk/vocab/resource/>
SELECT DISTINCT ?resource ?coords ?title ?descr ?from ?to ?typ
WHERE { 
?resource georss:point ?coords . 
?resource dct:title ?title .
?resource dct:abstract ?descr .
?resource ispin:availableFrom ?from .
?resource ispin:availableTo ?to .
?resource ispin:type ?typ .
}
";

$q4 = "PREFIX georss: <http://www.georss.org/georss/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX ispin: <http://opendata.tellmescotland.gov.uk/vocab/resource/>
SELECT DISTINCT ?resource ?coords ?url
WHERE { 
?resource georss:point ?coords . 
?resource dct:URI ?url .
}
LIMIT 5";

$rows = $store->query($q3, 'rows');

?>
<!doctype html>
<html>
<head><title>Public Notices</title></head>
<body>

	<div id="mapdiv" style="width:100%; height;60%"></div>

<?
if(isset($store->errors[0])){
	echo "<pre>";
	var_dump($store);
	echo "</pre>";
}
echo "<pre>";
var_dump($rows);
echo "</pre>";
$store->drop();
?>

</body>
</html>
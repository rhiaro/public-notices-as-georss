<?
include_once("lib/arc2/ARC2.php");

$config = array(
  'remote_store_endpoint' => 'http://opendata.tellmescotland.gov.uk/sparql',
);
$store = ARC2::getRemoteStore($config);

/* Query endpoint for notices */
$q = "PREFIX georss: <http://www.georss.org/georss/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX ispin: <http://opendata.tellmescotland.gov.uk/vocab/resource/>
SELECT DISTINCT ?resource ?coords ?title ?descr ?from ?to ?typ
WHERE { 
?resource georss:point ?coords . 
?resource dct:title ?title .
?resource dct:abstract ?descr .
?resource ispin:availableFrom ?from .
?resource ispin:availableTo ?to .
?resource ispin:type ?typ .
}
";
$rows = $store->query($q, 'rows');

?>
<!DOCTYPE HTML>
<html>
  <head>
    <title>Public Notices on OSM</title>
    <style type="text/css">
      html, body {
          width: 100%;
          height: 100%;
          margin: 0;
          background-color: #b5d0d0;
          word-wrap: break-word;
          font-family: Arial, Helvetica, sans-serif;
      }
      #basicMap {
        width:70%; height:100%;
        float: left;
      }
      .info {
          width:30%; height:100%; max-height: 100%;
          float: right;
          overflow: auto;
      }
      .inner {
        padding: 1em;
        background-color: white;
        margin: 1em;
      }
    </style>
    <script src="lib/map/OpenLayers.js"></script>
    <script>
      function init() {
        map = new OpenLayers.Map("basicMap");
        var mapnik         = new OpenLayers.Layer.OSM();
        var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
        var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
        var position       = new OpenLayers.LonLat(-3.12,55.57).transform( fromProjection, toProjection);
        var zoom           = 6; 
 
        map.addLayer(mapnik);

        var vectorLayer = new OpenLayers.Layer.Vector("Overlay");

        <?foreach($rows as $k => $row):?>
          <? $coords_ar = explode(" ", $row['coords']); ?>
          var feature = new OpenLayers.Feature.Vector(
                  new OpenLayers.Geometry.Point(<?=$coords_ar[1]?>,<?=$coords_ar[0]?>).transform(fromProjection, toProjection),
                  {
                      title:'<?=str_replace(array("\r", "\r\n", "\n"), ' ', str_replace('\'', '\\\'', $row['title']))?>'
                     ,description: '<?=str_replace(array("\r", "\r\n", "\n"), ' ', str_replace('\'', '\\\'', $row['descr']))?>'
                     ,url: '<?=$row['resource']?>'
                     ,from: '<?=$row['from']?>'
                     ,to: '<?=$row['to']?>'
                     ,typ: '<?=substr(strrchr($row['typ'],'/'),1)?>'
                  },
                  {externalGraphic: 'lib/map/img/marker.png', graphicHeight: 25, graphicWidth: 21, graphicXOffset:-12, graphicYOffset:-25 }
            );
          vectorLayer.addFeatures(feature);
        <?endforeach?>

        map.addLayer(vectorLayer);
        
        var selectFeature = new OpenLayers.Control.SelectFeature(vectorLayer);
        map.addControl(selectFeature);
        selectFeature.activate();

        vectorLayer.events.on({
          'featureselected': showInfo
        });


        map.setCenter(position, zoom);

        function showInfo(ft){
          //alert(ft.feature.attributes.description);
          var box = document.getElementById('info');
          var atts = ft.feature.attributes;
          if(atts.title.length < 1){
            var t = "[no title]";
          }else{
            var t = atts.title;
          }
          output = "<h2>" + t + "</h2>";
          output += "<h3>" + atts.url + "</h3>";
          output += "<p><strong>Type:</strong> " + atts.typ + "</p>";
          output += "<p><strong>Available from:</strong> " + atts.from + " </p><p><strong>Available to:</strong> " + atts.to + "</p>";
          output += atts.description; 

          box.innerHTML = output;
        }

        function hideInfo(ft){

        }
      }
    </script>
  </head>
  <body onload="init();">
    <div id="basicMap"></div>
    <div class="info">
      <div class="inner">
        <h1>Public Notices</h1>
        <p>From TellMeScotland.gov.uk</p>
        <p><a href="https://bitbucket.org/rhiaro/public-notices-as-georss" target="_blank">Code on Bitbucket</a></p>
      </div>
      <div class="inner" id="info">
      <?
        if(isset($store->errors[0]) || count($rows) < 1){
          echo "<p><strong>Error with the data source!</strong</p><p class=\"fail\">".$store->errors[0]."</p>";
          echo "<pre>";
          var_dump($store);
          echo "<hr/>";
          var_dump($rows);
          echo "</pre>";
        }else{
          echo "<p class=\"win\">Click a marker for more information.</p>";
        }
      ?>
      </div>
    </div>
  </body>
</html>